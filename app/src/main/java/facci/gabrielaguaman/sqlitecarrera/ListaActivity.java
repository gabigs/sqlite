package facci.gabrielaguaman.sqlitecarrera;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity {
    ListView lista_carrera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        lista_carrera = (ListView)findViewById(R.id.lista_carrera);

        List<Carrera> carreras = Carrera.listAll(Carrera.class);
        List<String> lista_Carreras = new ArrayList<>();

        for (Carrera carrera: carreras)
        {
            Log.e("Carrera", carrera.getApodo()+""+carrera.getPeso()+
                    ""+carrera.getAltura()+""+carrera.getCaballo()+""+carrera.getFecha());
            lista_Carreras.add(carrera.getApodo()+""+carrera.getPeso()+
                    ""+carrera.getAltura()+""+carrera.getCaballo()+""+carrera.getFecha());
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                        lista_Carreras);
        lista_carrera.setAdapter(itemsAdapter);
    }
}