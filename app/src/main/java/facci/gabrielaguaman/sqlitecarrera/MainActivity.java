package facci.gabrielaguaman.sqlitecarrera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    EditText apodo, peso, altura, caballo, fecha, consulta;
    Button ingresar, actualizar, eliminar, individual, general;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apodo = (EditText)findViewById(R.id.apodo);
        peso = (EditText)findViewById(R.id.peso);
        altura = (EditText)findViewById(R.id.altura);
        caballo = (EditText)findViewById(R.id.caballo);
        fecha = (EditText)findViewById(R.id.fecha);
        consulta = (EditText)findViewById(R.id.consulta);
        ingresar = (Button)findViewById(R.id.ingresar);
        actualizar = (Button)findViewById(R.id.actualizar);
        eliminar = (Button)findViewById(R.id.eliminar);
        individual = (Button)findViewById(R.id.individual);
        general = (Button)findViewById(R.id.general);

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Carrera carrera1 = new Carrera(apodo.getText().toString(), peso.getText().toString(), altura.getText().toString(),
                        caballo.getText().toString(), fecha.getText().toString());
                carrera1.save();
            }
        });

        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Carrera carrera2 = Carrera.findById(Carrera.class, Long.parseLong(
                        consulta.getText().toString()));
                carrera2.apodo= "Nuevo";
                carrera2.peso = "Nuevo";
                carrera2.altura = "Nuevo";
                carrera2.caballo = "Nuevo";
                carrera2.fecha = "Nuevo";
                carrera2.save();
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Carrera> list_Carrera = Carrera.listAll(Carrera.class);
                Carrera.deleteAll(Carrera.class);
            }
        });

        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Carrera carrera4 = Carrera.findById(Carrera.class, Long.parseLong(consulta.getText().toString()));
                apodo.setText(carrera4.getApodo());
                peso.setText(carrera4.getPeso());
                altura.setText(carrera4.getAltura());
                caballo.setText(carrera4.getCaballo());
                fecha.setText(carrera4.getFecha());
            }
        });

        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivity(intent);
            }
        });

    }
}
