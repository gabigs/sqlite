package facci.gabrielaguaman.sqlitecarrera;

import com.orm.SugarRecord;

public class Carrera extends SugarRecord <Carrera> {
    String apodo;
    String peso;
    String altura;
    String caballo;
    String fecha;

    public Carrera(){

    }

    public Carrera(String apodo, String peso, String altura, String caballo, String fecha) {
        this.apodo = apodo;
        this.peso = peso;
        this.altura = altura;
        this.caballo = caballo;
        this.fecha = fecha;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getCaballo() {
        return caballo;
    }

    public void setCaballo(String caballo) {
        this.caballo = caballo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
